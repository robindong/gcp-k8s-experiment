module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google"
  project_id                 = var.project_id
  region                     = var.region
  zones                      = var.zones
  name                       = "mmm-super"
  network                    = "mmm-super-dev-vpc"
  subnetwork                 = "mmm-super-dev-us-w1-subnet"
  ip_range_pods              = ""
  ip_range_services          = ""
  horizontal_pod_autoscaling = true

  node_pools = [
    {
      name               = "standard-1"
      machine_type       = "n1-standard-1"
      min_count          = 1
      max_count          = 20
      disk_size_gb       = 10
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = var.service_account
      preemptible        = true
      initial_node_count = 1
    },
  ]
}
