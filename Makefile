all: cluster node-pool

cluster:
	gcloud container clusters create mmm-super \
		--region us-west1-a \
		--network=mmm-super-dev-vpc \
		--subnetwork=mmm-super-dev-us-w1-subnet \
		--service-account=mmm-super-dev@gcp-wow-rwds-ai-mmm-super-dev.iam.gserviceaccount.com \
		--num-nodes=1

node-pool:
	gcloud container node-pools create standard-1 \
		--region=us-west1-a \
		--cluster=mmm-super \
		--disk-type="pd-standard" \
		--disk-size=10GB \
		--enable-autoscaling \
		--enable-autorepair \
		--image-type="COS" \
		--machine-type="n1-standard-1" \
		--node-labels="node_pool=standard-1-p" \
		--service-account=mmm-super-dev@gcp-wow-rwds-ai-mmm-super-dev.iam.gserviceaccount.com \
		--preemptible \
		--min-nodes=1 \
		--max-nodes=20 \
		--metadata disable-legacy-endpoints=true \
		--num-nodes=1

mmm-bws-node-pool: mmm-bws-node-pool-preemptible mmm-bws-node-pool-n

mmm-bws-node-pool-preemptible:
	for pair in "standard-2 e2-standard-2" "standard-4 e2-standard-4" "standard-64 n2-standard-64" "highmem-16 e2-highmem-16" "highmem-32 n2-highmem-32" "highcpu-32 n2-highcpu-32" "highcpu-64 n2-highcpu-64"; do \
		set -- $$pair; \
		gcloud container node-pools create $${1} \
			--region=us-west1-a \
			--cluster=decision-engine \
			--disk-type="pd-standard" \
			--disk-size=100GB \
			--enable-autoscaling \
			--enable-autorepair \
			--image-type="COS" \
			--machine-type="$${2}" \
			--node-labels="pool-name=$${1}-p" \
			--service-account=auto-ml@wx-bq-poc.iam.gserviceaccount.com \
			--preemptible \
			--min-nodes=0 \
			--max-nodes=20 \
			--metadata disable-legacy-endpoints=true \
			--num-nodes=0; \
	done

mmm-bws-node-pool-n:
	for pair in "standard-8 e2-standard-8"; do \
		set -- $$pair; \
		gcloud container node-pools create $${1} \
			--region=us-west1-a \
			--cluster=decision-engine \
			--disk-type="pd-standard" \
			--disk-size=100GB \
			--enable-autoscaling \
			--enable-autorepair \
			--image-type="COS" \
			--machine-type="$${2}" \
			--node-labels="pool-name=$${1}" \
			--service-account=auto-ml@wx-bq-poc.iam.gserviceaccount.com \
			--min-nodes=0 \
			--max-nodes=20 \
			--metadata disable-legacy-endpoints=true \
			--num-nodes=0; \
	done
